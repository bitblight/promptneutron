"""
promptneutron.config.bsd

"""

import io

default_comment_sigil = '#'
default_directive_separator = None


class ConfigReader(object):

    def __init__(self, file_like, comment_sigil=None,
            directive_separator=None):

        self._directives = self.__directives(file_like)

        if comment_sigil is None:
            comment_sigil = default_comment_sigil
        
        if directive_separator is None:
            directive_separator = default_directive_separator
        
        self.comment_sigil = comment_sigil
        self.directive_separator = directive_separator

    def __iter__(self):

        return self.read_directives()
    
    def parse_line(self, line):

        record, _ = self.split_comment(line)
        return self.split_directive(record)
    
    def read_directive(self):

        return next(self._directives)
    
    def read_directives(self):

        for directive in self._directives:
            yield directive

    def split_comment(self, line):

        return self.__split(line, self.comment_sigil)

    def split_directive(self, directive):

        directive, parameters = self.__split(directive,
            self.directive_separator)
        
        if len(directive) == 0:
            directive = None
        
        return (directive, parameters)
    
    def __directives(self, f):

        return filter(
            lambda record: record[0] is not None,
            (self.parse_line(line)
                for line in filter(lambda line: len(line) > 0,
                    (line.strip() for line in f))))
    
    def __split(self, text, separator):

        tokens = text.split(separator, maxsplit=1)

        try:
            key, value = tokens
        except ValueError:
            key, value = text.strip(), None
        else:
            key, value = key.strip(), value.strip()

        return (key, value)


def reader(file_like_or_str, *args, **kwargs):

    if isinstance(file_like_or_str, str):
        file_like_or_str = io.StringIO(file_like_or_str)

    return ConfigReader(file_like_or_str, *args, **kwargs)
